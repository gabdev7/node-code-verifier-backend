import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";

// Confiracion the .env file
dotenv.config();

// Create Express APP
const app: Express = express();
const port: string | number = process.env.PORT || 8000;

// Define the first Route of APP
app.get("/",(req: Request, res: Response) => {
    // Send Hello 
    res.send("Welcome to Express Backend")
});

// Define the first Route of APP
app.get("/hello",(req: Request, res: Response) => {
    // Send Hello 
    res.send("Welcome to hello")
});

// Execute App and Listen Ports
app.listen(port, () =>{
    console.log("Express Server Running at http://localhost/8000")
})